package gov.ic.ngic.cjdb.javatools.persistence.graph.mongo;

import gov.ic.ngic.cjdb.javatools.util.ReflectionUtils;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.Id;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

public abstract class BaseService {
    private MongoTemplate template;

    public BaseService(@NonNull final MongoTemplate template) {
        this.template = template;
    }

    public <T> Optional<T> getById(@NonNull final Object id, @NonNull final Class<T> type) {
        final T object = template.findById(id, type);

        if (object != null) {
            reconstructObjectGraph(object);
        }

        return Optional.ofNullable(object);
    }

    public <T> List<T> getAll(@NonNull final Class<T> type) {
        return template.findAll(type).stream()
                .map(obj -> type.cast(reconstructObjectGraph(obj)))
                .collect(Collectors.toList());
    }

    private Object reconstructObjectGraph(@NonNull final Object object) {
        final Map<Object, Object> unvisitedObjects = new HashMap<>();
        final Map<Object, Object> visitedObjects = new HashMap<>();

        unvisitedObjects.put(ReflectionUtils.Persistence.extractId(object), object);

        while (!unvisitedObjects.isEmpty()) {
            final Object id = unvisitedObjects.keySet().iterator().next();
            final Object target = unvisitedObjects.remove(id);
            visitedObjects.put(id, target);

            ReflectionUtils.findFieldsWithAnnotation(target.getClass(), IdField.class)
                    .stream()
                    .filter(field -> !field.getAnnotation(IdField.class).lazy() && !ReflectionUtils.isNullOrEmpty(field, target))
                    .forEach(field -> findAndAddObjects(target, field, unvisitedObjects, visitedObjects));
        }

        return object;
    }

    private void findAndAddObjects(
            @NonNull final Object object,
            @NonNull final Field field,
            @NonNull final Map<Object, Object> unvisitedObjects,
            @NonNull final Map<Object, Object> visitedObjects) {
        final List<Object> ids = ReflectionUtils.listify(field, object);

        final IdField idFieldAnnotation = field.getAnnotation(IdField.class);

        final String targetFieldName = idFieldAnnotation.value();
        final Field targetField = ReflectionUtils.findField(object.getClass(), targetFieldName);

        if (targetField == null) {
            throw new IllegalArgumentException(targetFieldName + " does not exist on " + object.getClass().getSimpleName());
        }

        ReflectionUtils.makeAccessible(targetField);

        if (ReflectionUtils.isCollection(targetField) != ReflectionUtils.isCollection(field)) {
            throw new IllegalArgumentException(targetFieldName + " and " + field.getName() + " should both be collections or neither can be collections");
        }

        ReflectionUtils.setObjectsInField(targetField, object, ids.stream()
                .map(id -> resolveObject(id, resolveType(targetField, idFieldAnnotation), unvisitedObjects, visitedObjects))
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));
    }

    @Nullable
    private Object resolveObject(@NonNull final Object id, @NonNull final Class<?> type, @NonNull final Map<Object, Object> unvisitedObjects, @NonNull final Map<Object, Object> visitedObjects) {
        if (visitedObjects.containsKey(id)) {
            return visitedObjects.get(id);
        }

        if (!unvisitedObjects.containsKey(id)) {
            final Object obj = template.findById(id, type);

            if (obj == null) {
                return null;
            }

            unvisitedObjects.put(id, obj);
        }

        return unvisitedObjects.get(id);
    }

    private Class<?> resolveType(@NonNull final Field field, @NonNull final IdField annotation) {
        if (ReflectionUtils.isCollection(field)) {
            if (annotation.type().equals(Object.class)) {
                throw new IllegalArgumentException("Unable to resolve " + field.getName() + " type... please annotate type on all collection fields");
            }

            return annotation.type();
        }

        return field.getType();
    }

    @SuppressWarnings("unchecked")
    public <T> T save(@NonNull final T object) {
        final Map<Object, Object> persistedObjects = new HashMap<>();
        final List<Object> traversedObjects = new ArrayList<>();
        final Deque<Object> objectStack = new ArrayDeque<>();
        objectStack.add(object);

        while (!objectStack.isEmpty()) {
            final Object target = objectStack.pop();
            traversedObjects.add(target);

            ReflectionUtils.findFieldsWithAnnotation(target.getClass(), Transient.class)
                    .forEach(field -> {
                        ReflectionUtils.makeAccessible(field);
                        persist(field, target, persistedObjects);

                        objectStack.addAll(
                                ReflectionUtils.listify(field, target).stream()
                                        .filter(obj -> !traversedObjects.contains(obj))
                                        .collect(Collectors.toList())
                        );
                    });

            persist(target, persistedObjects);
        }

        return (T) persistedObjects.get(object);
    }

    private void persist(@NonNull final Field field, @NonNull final Object object, @NonNull final Map<Object, Object> persistedObjects) {
        final List<Object> ids = ReflectionUtils.listify(field, object)
                .stream()
                .map(obj -> persist(obj, persistedObjects))
                .collect(Collectors.toList());

        if (ids.size() == 0) {
            return;
        }

        final Field idField = ReflectionUtils.findFieldsWithAnnotation(object.getClass(), IdField.class)
                .stream()
                .filter(f -> f.getAnnotation(IdField.class).value().equals(field.getName()))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Unable to link " + field.getName() + " with a corresponding annotated IdField"));

        ReflectionUtils.makeAccessible(idField);

        try {
            if (ReflectionUtils.isCollection(idField)) {
                idField.set(object, ids);
            } else {
                idField.set(object, ids.get(0));
            }
        } catch (IllegalAccessException ignored) { }
    }

    @Nullable
    private Object persist(@NonNull final Object object, @NonNull final Map<Object, Object> persistedObjects) {
        if (object == null) {
            return null;
        }

        if (persistedObjects.containsKey(object)) {
            return extractIdFromObject(object);
        }

        if (!template.collectionExists(object.getClass())) {
            template.createCollection(object.getClass());
        }

        persistedObjects.put(object, template.save(object));

        return extractIdFromObject(persistedObjects.get(object));
    }

    private Object extractIdFromObject(@NonNull final Object object) {
        try {
            final Field idField =  ReflectionUtils.findFieldsWithAnnotation(object.getClass(), Id.class)
                    .stream()
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException(object.getClass() + " does not have JavaX Id field"));

            ReflectionUtils.makeAccessible(idField);

            return idField.get(object);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public void delete(@NonNull final Object object) {
        if (!template.collectionExists(object.getClass())) {
            return;
        }

        final Deque<Object> objects = new ArrayDeque<>();
        final List<Object> deletedObjects = new ArrayList<>();
        objects.add(object);

        while (!objects.isEmpty()) {
            final Object target = objects.pop();

            template.remove(target);
            deletedObjects.add(target);

            objects.addAll(getCascadedChildren(target, deletedObjects));
        }
    }

    private List<Object> getCascadedChildren(@NonNull final Object object, @NonNull final List<Object> visitedObjects) {
        return ReflectionUtils.findFieldsWithAnnotation(object.getClass(), IdField.class)
                .stream()
                .filter(field -> field.getAnnotation(IdField.class).cascadeDelete())
                .map(field -> ReflectionUtils.getField(object.getClass(), field.getAnnotation(IdField.class).value()))
                .flatMap(field -> {
                    ReflectionUtils.makeAccessible(field);

                    return ReflectionUtils.listify(field, object).stream()
                            .filter(obj -> !visitedObjects.contains(obj));
                }).collect(Collectors.toList());
    }

    public MongoTemplate getTemplate() {
        return template;
    }
}
