package gov.ic.ngic.cjdb.javatools.util;

import gov.ic.ngic.cjdb.javatools.persistence.graph.LinkedRepository;
import org.springframework.context.ApplicationContext;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.lang.NonNull;
import org.springframework.util.CollectionUtils;

import javax.persistence.Id;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ReflectionUtils extends org.springframework.util.ReflectionUtils {
    public static boolean isCollection(@NonNull final Field field) {
        makeAccessible(field);

        return Collection.class.isAssignableFrom(field.getType());
    }

    public static boolean isString(@NonNull final Field field) {
        makeAccessible(field);

        return String.class.isAssignableFrom(field.getType());
    }

    public static boolean isNullOrEmpty(@NonNull final Field field, @NonNull final Object object) {
        makeAccessible(field);

        if (isString(field)) {
            try {
                return StringUtils.isNullOrEmpty((String) field.get(object));
            } catch (IllegalAccessException ignored) {
                return true;
            }
        }

        if (isCollection(field)) {
            try {
                return CollectionUtils.isEmpty((Collection<?>) field.get(object));
            } catch (IllegalAccessException ignored) {
                return true;
            }
        }

        try {
            return field.get(object) == null;
        } catch (IllegalAccessException ignored) {
            return true;
        }
    }

    public static Type getTypeGenerically(@NonNull final Field field) {
        makeAccessible(field);

        if (isCollection(field)) {
            return field.getGenericType();
        }

        return new Type() {
            @Override
            public String getTypeName() {
                return field.getType().getTypeName();
            }
        };
    }

    public static List<Object> listify(@NonNull final Field field, @NonNull final Object object) {
        makeAccessible(field);

        if (isNullOrEmpty(field, object)) {
            return Collections.emptyList();
        }

        if (isCollection(field)) {
            try {
                return new ArrayList<Object>((Collection) field.get(object));
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        } else {
            try {
                return Collections.singletonList(field.get(object));
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static List<Field> findFieldsWithAnnotation(@NonNull final Class<?> target, @NonNull final Class<? extends Annotation> annotation) {
        return getAllFields(target)
                .stream()
                .filter(field -> field.isAnnotationPresent(annotation))
                .collect(Collectors.toList());
    }

    public static List<Object> getObjectsFromField(@NonNull final Object object, @NonNull final Class<? extends Annotation> annotation) {
        return findFieldsWithAnnotation(object.getClass(), annotation)
                .stream()
                .flatMap(field -> listify(field, object).stream())
                .collect(Collectors.toList());
    }

    public static List<Field> getAllFields(@NonNull final Class<?> type) {
        return getAllFields(type, Object.class);
    }

    public static List<Field> getAllFields(@NonNull final Class<?> type, @NonNull final Class<?> exclusiveParent) {
        Class<?> target = type;
        final List<Field> fields = new ArrayList<>();

        while (!target.equals(exclusiveParent)) {
            Collections.addAll(fields, target.getDeclaredFields());

            target = target.getSuperclass();
        }

        return fields;
    }

    public static Field getField(@NonNull final Class<?> type, @NonNull final String name){
        Class<?> target = type;

        while (!target.equals(Object.class)) {
            try {
                return target.getDeclaredField(name);
            } catch (NoSuchFieldException e) {
                target = target.getSuperclass();
            }
        }

        throw new IllegalArgumentException("Unable to resolve field " + name + " on class " + type.getSimpleName());
    }

    @SuppressWarnings("unchecked")
    public static void setObjectsInField(@NonNull final Field field, @NonNull final Object object, @NonNull final List<Object> objects) {
        if (isCollection(field)) {
            try {
                Collection collection = (Collection) field.get(object);

                if (collection == null) {
                    collection = (Collection) field.getType().newInstance();
                }

                collection.clear();
                collection.addAll(objects);
            } catch (IllegalAccessException | InstantiationException e) {
                throw new RuntimeException(e);
            }
        } else {
            final Object obj = objects.size() == 0 ? null : objects.get(0);

            try {
                field.set(object, obj);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static class Persistence {
        static Field findId(@NonNull final Object object) throws IllegalArgumentException {
            return findFieldsWithAnnotation(object.getClass(), Id.class)
                    .stream()
                    .findFirst()
                    .orElseThrow(IllegalArgumentException::new);
        }

        public static Object extractId(@NonNull final Object object) throws IllegalArgumentException {
            final Field field = findId(object);
            makeAccessible(field);

            try {
                return field.get(object);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }

        public static MongoRepository resolveRepository(@NonNull final ApplicationContext context, @NonNull final Class<?> type) {
            final LinkedRepository annotation = type.getAnnotation(LinkedRepository.class);

            if (annotation != null) {
                if (!annotation.repository().equals(Object.class)) {
                    return (MongoRepository) context.getAutowireCapableBeanFactory().getBean(annotation.repository());
                }

                return (MongoRepository) context.getAutowireCapableBeanFactory().getBean(annotation.repositoryName().substring(0, 1) + annotation.repositoryName().substring(1));
            } else {
                final String className = type.getSimpleName().substring(0, 1).toLowerCase() + type.getSimpleName().substring(1) + "Repository";
                return (MongoRepository) context.getAutowireCapableBeanFactory().getBean(className.substring(0, 1).toLowerCase() + className.substring(1));
            }
        }
    }

    @FunctionalInterface
    public interface FunctionalExceptionHandler<T, R> {
        R handle(T t) throws Exception;
    }

    public static class FunctionExceptionHandlerMapper {
        public static <T, R> Function<T, R> handle(@NonNull final FunctionalExceptionHandler<T, R> function) {
            return t -> {
                try {
                    return function.handle(t);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            };
        }
    }
}
