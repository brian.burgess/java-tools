package gov.ic.ngic.cjdb.javatools.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.lang.NonNull;

import java.util.Optional;
import java.util.stream.Stream;

public class ContextUtils {
    public static Optional<ObjectMapper> getDefaultObjectMapper(@NonNull final ApplicationContext context) {
        return Stream.of(context.getBeanNamesForType(ObjectMapper.class))
                .map(name -> (ObjectMapper) context.getBean(name))
                .findFirst();
    }
}
