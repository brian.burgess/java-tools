package gov.ic.ngic.cjdb.javatools.test;

import org.junit.rules.ExternalResource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.MongoPersistentEntityIndexResolver;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.util.ClassTypeInformation;
import org.springframework.data.util.TypeInformation;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.util.function.Consumer;
import java.util.stream.Stream;

public class MongoCleanupRule extends ExternalResource {
    public interface MongoIntegrationTest {
        MongoTemplate getMongoTemplate();
    }

    private final MongoIntegrationTest testClass;
    private final Class<?>[] collectionClasses;

    public MongoCleanupRule(@NonNull final MongoIntegrationTest testClass, @NonNull final Class<?>... collectionClasses) throws IllegalArgumentException {
        this.testClass = testClass;
        this.collectionClasses = collectionClasses;

        Stream.of(collectionClasses).forEach(collection -> Assert.isTrue(
                collection.isAnnotationPresent(Document.class),
                "All collection classes passed in must be annotated with the document annotation")
        );
    }

    @Override
    protected void before() {
        dropCollections();
        createIndices();
    }

    @Override
    protected void after() {
        dropCollections();
    }

    private void dropCollections() {
        Stream.of(collectionClasses).forEach(collection -> getMongoTemplate().dropCollection(collection));
    }

    private void createIndices() {
        Stream.of(collectionClasses).forEach(this::createIndicesFor);
    }

    private void createIndicesFor(@NonNull final Class<?> collection) {
        new MongoPersistentEntityIndexResolver((MongoMappingContext) getMongoTemplate().getConverter().getMappingContext())
                .resolveIndexFor(getTypeInformation(collection))
                .forEach(
                        (Consumer<MongoPersistentEntityIndexResolver.IndexDefinitionHolder>) indexDefinitionHolder ->
                                getMongoTemplate().getDb().getCollection(getCollectionName(collection)).createIndex(indexDefinitionHolder.getIndexKeys())
                );
    }

    private TypeInformation<?> getTypeInformation(final Class<?> collectionClass) {
        return ClassTypeInformation.from(collectionClass);
    }

    private String getCollectionName(final Class<?> collectionClass) {
        if (StringUtils.hasText(collectionClass.getAnnotation(Document.class).value())) {
            return collectionClass.getAnnotation(Document.class).value();
        }

        return collectionClass.getSimpleName();
    }

    private MongoTemplate getMongoTemplate() {
        return testClass.getMongoTemplate();
    }
}
