package gov.ic.ngic.cjdb.javatools.persistence.unstructured;

import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.lang.NonNull;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractUnstructuredService {
    private final MongoTemplate mongoTemplate;

    public AbstractUnstructuredService(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public Map<Object, Object> get() {
        for (final Document document : getCollection(getCollectionName()).find()) {
            if (document.containsKey(getCollectionName())) {
                return document.get(getCollectionName(), new HashMap<>());
            }
        }

        return new HashMap<>();
    }

    public Document save(@NonNull final Map<?, ?> tree) {
        final MongoCollection<Document> collection = getCollection(getCollectionName());

        final Map<String, Object> map = new HashMap<>();
        map.put(getCollectionName(), tree);

        final Document document = new Document(map);

        collection.insertOne(new Document(map));

        return document;
    }

    public void clear() {
        mongoTemplate.dropCollection(getCollectionName());
    }

    private MongoCollection<org.bson.Document> getCollection(@NonNull final String collectionName) {
        if (mongoTemplate.collectionExists(collectionName)) {
            return mongoTemplate.getCollection(collectionName);
        }

        return mongoTemplate.createCollection(collectionName);
    }

    @NonNull
    protected abstract String getCollectionName();
}
