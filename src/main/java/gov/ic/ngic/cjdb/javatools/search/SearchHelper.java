package gov.ic.ngic.cjdb.javatools.search;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.Map;

public class SearchHelper {
    public static <T> List<T> search(@NonNull final Map<String, String> fieldQuery, @NonNull final MongoTemplate template, @NonNull final Class<T> type) {
        return search(fieldQuery, template, false, type);
    }

    public static <T> List<T> search(@NonNull final Map<String, String> fieldQuery, @NonNull final MongoTemplate template, final boolean strict, @NonNull final Class<T> type) {
        final org.springframework.data.mongodb.core.query.Query query = new org.springframework.data.mongodb.core.query.Query();

        fieldQuery.forEach((key, value) -> query.addCriteria(Criteria.where(key).regex((strict ? "^" : "*") + value + (strict ? "$" : "*"))));

        return template.find(query, type);
    }
}
