package gov.ic.ngic.cjdb.javatools.util;

import org.springframework.lang.Nullable;

import java.util.Objects;
import java.util.Random;

public class StringUtils extends org.springframework.util.StringUtils {
    public static boolean isNullOrEmpty(@Nullable final String string) {
        return Objects.isNull(string) || StringUtils.isEmpty(string);
    }

    public static String random(final int length) {
        return random(length, true, true);
    }

    public static String random(final int length, final boolean allowChars, final boolean allowNums) {
        String string = "";

        if (!allowChars && !allowNums) {
            return string;
        }

        if (allowChars) {
            string = "abcdefghijklmnopqrstuvwyz";

            string = string + string.toUpperCase();
        }

        if (allowNums) {
            string =string + "0123456789";
        }

        final StringBuilder builder = new StringBuilder();
        final Random random = new Random();

        while (builder.length() < length) {
            builder.append(string.charAt(random.nextInt(string.length() -1)));
        }

        return builder.toString();
    }
}
