package gov.ic.ngic.cjdb.javatools.filter;

import org.springframework.lang.NonNull;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

public abstract class MutableHttpFilter extends HttpFilter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        chain.doFilter(
                request.getClass().isAssignableFrom(HttpServletRequest.class) ? doFilter(cast(request)) : request,
                response.getClass().isAssignableFrom(HttpServletResponse.class) ? doFilter(cast(response)) : response
        );
    }

    private MutableHttpServletRequest cast(ServletRequest request) {
        if (request instanceof MutableHttpServletRequest) {
            return (MutableHttpServletRequest) request;
        }

        return new MutableHttpServletRequest((HttpServletRequest) request);
    }

    private HttpServletResponseWrapper cast(ServletResponse response) {
        if (response instanceof HttpServletResponseWrapper) {
            return (HttpServletResponseWrapper) response;
        }

        return new HttpServletResponseWrapper((HttpServletResponse) response);
    }

    @NonNull
    protected MutableHttpServletRequest doFilter(@NonNull final MutableHttpServletRequest request) {
        return request;
    }

    @NonNull
    protected HttpServletResponseWrapper doFilter(@NonNull final HttpServletResponseWrapper response) {
        return response;
    }
}
