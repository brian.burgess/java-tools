package gov.ic.ngic.cjdb.javatools.filter;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

class HeaderFactory {
    static Map<String, String> create(@NonNull final HttpServletRequest request) {
        return create(new HttpEntity(request));
    }

    static Map<String, String> create(@NonNull final HttpServletResponse response) {
        return create(new HttpEntity(response));
    }

    private static Map<String, String> create(@NonNull final HttpEntity entity) {
        final Map<String, String> headers = new HashMap<>();

        entity.getHeaderNames()
                .forEach(header -> headers.put(header, Optional.ofNullable(entity.getHeader(header)).orElse("")));

        return headers;
    }

    @SuppressWarnings("ConstantConditions")
    private static class HttpEntity {
        private final HttpServletRequest request;
        private final HttpServletResponse response;

        HttpEntity(@NonNull final HttpServletRequest request) {
            this.request = request;
            response = null;
        }

        HttpEntity(@NonNull final HttpServletResponse response) {
            request = null;
            this.response = response;
        }

        @Nullable
        String getHeader(String name) {
            if (request != null) {
                return request.getHeader(name);
            }

            return response.getHeader(name);
        }

        List<String> getHeaderNames() {
            if (request != null) {
                return Collections.list(request.getHeaderNames());
            }

            return new ArrayList<>(response.getHeaderNames());
        }
    }
}
