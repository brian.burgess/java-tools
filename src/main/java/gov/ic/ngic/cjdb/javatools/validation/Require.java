package gov.ic.ngic.cjdb.javatools.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface Require {
    String[] ifFieldsPresent() default {};
    String[] ifFieldsNotPresent() default {};
}
