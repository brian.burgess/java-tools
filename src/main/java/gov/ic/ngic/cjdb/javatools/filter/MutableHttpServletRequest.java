package gov.ic.ngic.cjdb.javatools.filter;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.*;

public class MutableHttpServletRequest extends HttpServletRequestWrapper {
    private final Map<String, String> headers;
    private final Map<String, String[]> requestParams = new HashMap<>();

    MutableHttpServletRequest(HttpServletRequest request) {
        super(request);
        headers = HeaderFactory.create(request);
        requestParams.putAll(request.getParameterMap());
    }

    public void putHeader(String name, String value){
        headers.put(name, value);
    }

    public boolean hasHeader(@NonNull final String name) {
        return headers.get(name) != null;
    }

    @Nullable
    public String getHeader(String name) {
        return headers.get(name);
    }

    public Enumeration<String> getHeaderNames() {
        return Collections.enumeration(headers.keySet());
    }

    public void putRequestParam(@NonNull final String key, @NonNull final String value) {
        requestParams.put(key, new String[]{value});
    }

    public void putRequestParam(@NonNull final String key, @NonNull final String[] value) {
        requestParams.put(key, value);
    }

    @Override
    public String getParameter(String name) {
        final String[] string = Optional.ofNullable(requestParams.get(name))
                .orElseGet(() -> new String[0]);

        if (string.length == 0) {
            return "";
        } else if (string.length == 1) {
            return string[0];
        } else {
            final StringBuilder builder = new StringBuilder();

            for (String s : string) {
                builder.append(s);
            }

            return builder.toString();
        }
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        return requestParams;
    }

    @Override
    public Enumeration<String> getParameterNames() {
        return Collections.enumeration(requestParams.keySet());
    }

    @Override
    public String[] getParameterValues(String name) {
        return Optional.of(requestParams.get(name)).orElse(new String[0]);
    }
}
