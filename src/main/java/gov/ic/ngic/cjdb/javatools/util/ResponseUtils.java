package gov.ic.ngic.cjdb.javatools.util;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.lang.NonNull;

import java.util.List;

public class ResponseUtils {
    public static <T> Page<T> paginate(@NonNull final List<T> collections) {
        if (collections.isEmpty()) {
            return Page.empty();
        } else {
            return PageableExecutionUtils.getPage(collections, PageRequest.of(0, collections.size()), collections::size);
        }
    }

    public static <T> Page<T> paginate(@NonNull final List<T> collections, final int page, final int count) {
        if (collections.isEmpty()) {
            return Page.empty();
        } else {
            return PageableExecutionUtils.getPage(collections, PageRequest.of(page, count), collections::size);
        }
    }
}
