package gov.ic.ngic.cjdb.javatools.persistence.graph;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface LinkedRepository {
    Class<?> repository() default Object.class;
    String repositoryName() default "";
}
