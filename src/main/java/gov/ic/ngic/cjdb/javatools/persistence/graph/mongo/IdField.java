package gov.ic.ngic.cjdb.javatools.persistence.graph.mongo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface IdField {
    String value();
    Class<?> type() default Object.class;
    boolean cascadeDelete() default false;
    boolean lazy() default false;
}
