package gov.ic.ngic.cjdb.javatools.util;

public final class ExtendedMath {
    public static double round(double value, double precision) {
        double remainder = value%precision;

        if (remainder < precision/2) {
            return value-remainder;
        } else {
            return value+precision-remainder;
        }
    }

    public static float round(float value, float precision) {
        float remainder = value%precision;

        if (remainder < precision/2) {
            return value-remainder;
        } else {
            return value+precision-remainder;
        }
    }

    public static int round(int value, int precision) {
        int remainder = value%precision;

        if (remainder < precision/2f) {
            return value-remainder;
        } else {
            return value+precision-remainder;
        }
    }

    public static long round(long value, long precision) {
        long remainder = value%precision;

        if (remainder < precision/2f) {
            return value-remainder;
        } else {
            return value+precision-remainder;
        }
    }

    public static double ceil(double value, double precision) {
        return value+precision-(value%precision);
    }

    public static float ceil(float value, float precision) {
        return value+precision-(value%precision);
    }

    public static int ceil(int value, int precision) {
        return value+precision-(value%precision);
    }

    public static long ceil(long value, long precision) {
        return value+precision-(value%precision);
    }

    public static double floor(double value, double precision) {
        return value-(value%precision);
    }

    public static float floor(float value, float precision) {
        return value-(value%precision);
    }

    public static int floor(int value, int precision) {
        return value-(value%precision);
    }

    public static long floor(long value, long precision) {
        return value-(value%precision);
    }
}
