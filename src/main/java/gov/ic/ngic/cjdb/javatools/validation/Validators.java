package gov.ic.ngic.cjdb.javatools.validation;

import gov.ic.ngic.cjdb.javatools.util.ReflectionUtils;
import gov.ic.ngic.cjdb.javatools.util.StringUtils;
import org.springframework.lang.NonNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.*;
import java.util.regex.Pattern;

public class Validators {
    public static List<ValidationError> validate(@NonNull final Object object) {
        final Deque<Object> objectStack = new ArrayDeque<>();
        final List<Object> validatedObjects = new ArrayList<>();
        final List<ValidationError> errors = new LinkedList<>();

        objectStack.add(object);

        while (!objectStack.isEmpty()) {
            final Object obj = objectStack.pop();

            isValid(obj, errors);

            validatedObjects.add(obj);
            objectStack.addAll(getChildren(obj, validatedObjects));
        }

        return errors;
    }

    private static void isValid(@NonNull final Object object, @NonNull final List<ValidationError> errors) {
        ReflectionUtils.getAllFields(object.getClass()).forEach(field -> Validators.isInvalid(field, object, errors));
    }

    private static void isInvalid(@NonNull final Field field, @NonNull final Object object, @NonNull final List<ValidationError> errors) {
        ReflectionUtils.makeAccessible(field);

        if (field.isAnnotationPresent(Require.class)) {
            validateRequires(object, field, errors);
        }

        if (field.isAnnotationPresent(Min.class)) {
            validateMin(object, field, errors);
        }

        if (field.isAnnotationPresent(Max.class)) {
            validateMax(object, field, errors);
        }

        if (field.isAnnotationPresent(RegEx.class)) {
            validateRegex(object, field, errors);
        }

        if (field.isAnnotationPresent(CustomValidator.class)) {
            customValidation(object, field, errors);
        }
    }

    private static void validateRequires(@NonNull final Object object, @NonNull final Field field, @NonNull final List<ValidationError> errors) {
        final Require requires = field.getAnnotation(Require.class);

        final boolean isNull = ReflectionUtils.isNullOrEmpty(field, object);

        final List<Field> otherFields = ReflectionUtils.getAllFields(object.getClass());

        if (requires.ifFieldsPresent().length > 0) {
            for (final Field f: otherFields) {
                for (final String string: requires.ifFieldsPresent()) {
                    if (string.equals(f.getName()) && isNull && !ReflectionUtils.isNullOrEmpty(f, object)) {
                        errors.add(buildError(requires, object, field, "Is a required field if " + f.getName() + " is not blank"));
                    }
                }
            }
        } else if (requires.ifFieldsNotPresent().length > 0 && isNull) {
            boolean otherFieldNull = true;

            fieldScan:
            for (final Field f: otherFields) {
                for (int i = 0; i < requires.ifFieldsNotPresent().length; i++) {
                    if (requires.ifFieldsNotPresent()[i].equals(f.getName())) {
                        if (!ReflectionUtils.isNullOrEmpty(f, object)) {
                            otherFieldNull = false;
                            break fieldScan;
                        }
                    }
                }
            }

            if (otherFieldNull) {
                final StringBuilder builder = new StringBuilder("Is a required field if ");

                for (int i = 0; i < otherFields.size(); i++) {
                    builder.append(otherFields.get(i).getName());

                    if (i != otherFields.size() - 1) {
                        builder.append(" or ");
                    }
                }

                builder.append(" is empty");

                errors.add(buildError(requires, object, field, builder.toString()));
            }
        } else if (isNull) {
            errors.add(buildError(requires, object, field, "Is required"));
        }
    }

    private static void validateMin(@NonNull final Object object, @NonNull final Field field, @NonNull final List<ValidationError> errors) {
        final Min min = field.getAnnotation(Min.class);
        final Object obj;

        if (ReflectionUtils.isNullOrEmpty(field, object)) {
            if (min.require()) {
                errors.add(buildError(min, object, field, getErrorMessageForMin(min.value(), field)));
                return;
            }
        }

        try {
            obj = field.get(object);
        } catch (IllegalAccessException ignored) {
            return;
        }

        if (getInt(obj) < min.value()) {
            errors.add(buildError(min, object, field, getErrorMessageForMin(min.value(), field)));
        }
    }

    private static void validateMax(@NonNull final Object object, @NonNull final Field field, @NonNull final List<ValidationError> errors) {
        final Max max = field.getAnnotation(Max.class);
        final Object obj;

        if (ReflectionUtils.isNullOrEmpty(field, object)) {
            if (max.require()) {
                errors.add(buildError(max, object, field, getErrorMessageForMax(max.value(), field)));
                return;
            }
        }

        try {
            obj = field.get(object);
        } catch (IllegalAccessException ignored) {
            return;
        }

        if (getInt(obj) > max.value()) {
            errors.add(buildError(max, object, field, getErrorMessageForMax(max.value(), field)));
        }
    }

    private static void validateRegex(@NonNull final Object object, @NonNull final Field field, @NonNull final List<ValidationError> errors) {
        final RegEx expression = field.getAnnotation(RegEx.class);
        final Object obj;

        try {
            obj = field.get(object);
        } catch (IllegalAccessException ignored) {
            return;
        }

        if (!ReflectionUtils.isString(field)) {
            throw new IllegalArgumentException("RegEx annotation only belongs on string fields");
        }

        final String str = (String) obj;

        if (StringUtils.isEmpty(str)) {
            if (expression.required()) {
                errors.add(buildError(expression, object, field, "Is required and required to fit the pattern"));
            } else {
                return;
            }
        }

        if (!Pattern.compile(expression.value()).matcher(str).matches()) {
            errors.add(buildError(expression, object, field, "Is required to fit the pattern"));
        }
    }

    private static void customValidation(@NonNull final Object object, @NonNull final Field field, @NonNull final List<ValidationError> errors) {
        ReflectionUtils.makeAccessible(field);

        try {
            field.getAnnotation(CustomValidator.class).value().newInstance().validate(object, field, errors);
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private static int getInt(@NonNull final Object object) {
        if (object instanceof Number) {
            return (int) object;
        }

        if (Collection.class.isAssignableFrom(object.getClass())) {
            return ((Collection) object).size();
        }

        if (String.class.isAssignableFrom(object.getClass())) {
            return ((String) object).length();
        }

        return -1;
    }

    private static String getErrorMessageForMin(final int value, @NonNull final Field field) {
        if (ReflectionUtils.isCollection(field)) {
            return "Should have more than " + value + " children";
        } else if (ReflectionUtils.isString(field)) {
            return "Should have more than " + value + " characters";
        } else {
            return "Should be greater than or equal to " + value;
        }
    }

    private static String getErrorMessageForMax(final int value, @NonNull final Field field) {
        if (ReflectionUtils.isCollection(field)) {
            return "Should have less than " + value + " children";
        } else if (ReflectionUtils.isString(field)) {
            return "Should have less than " + value + " characters";
        } else {
            return "Should be less than or equal to " + value;
        }
    }

    @SuppressWarnings("unchecked")
    private static List<Object> getChildren(@NonNull final Object object, @NonNull final List<Object> validatedObjects) {
        final List<Object> children = new ArrayList<>();

        for (final Field field: ReflectionUtils.getAllFields(object.getClass())) {
            ReflectionUtils.makeAccessible(field);

            Object child;

            try {
                child = field.get(object);
            } catch (IllegalAccessException ignored) {
                continue;
            }

            if (child == null) {
                continue;
            }

            if (ReflectionUtils.isCollection(field)) {
                final Collection<Object> collection = (Collection<Object>) child;

                if (collection.size() > 0 && isInPackage(collection.toArray()[0])) {
                    collection.iterator().forEachRemaining(obj -> {
                        if (!validatedObjects.contains(obj)) {
                            children.add(obj);
                        }
                    });
                }

                continue;
            }

            if (isInPackage(child) && !validatedObjects.contains(child)) {
                children.add(child);
            }
        }

        return children;
    }

    private static boolean isInPackage(@NonNull final Object object) {
        return object.getClass().getPackage().getName().contains("gov.ic.ngic");
    }

    private static ValidationError buildError(@NonNull final Annotation annotation, @NonNull final Object object, @NonNull final Field field, @NonNull final String shortDescription) {
        final ValidationError error = new ValidationError(annotation, object.getClass(), field);
        error.setMessage("Error on " + object.getClass().getSimpleName() + " in field " + field.getName() + ": " + shortDescription);

        return error;
    }
}
