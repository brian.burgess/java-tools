package gov.ic.ngic.cjdb.javatools.validation;

import org.springframework.lang.NonNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class ValidationError {
    private final Annotation annotation;
    private final Class<?> invalidClass;
    private final Field invalidField;
    private String message = "";

    public ValidationError(@NonNull final Annotation annotation, @NonNull final Class<?> invalidClass, @NonNull final Field field) {
        this.annotation = annotation;
        this.invalidClass = invalidClass;
        this.invalidField = field;
    }

    public Annotation getAnnotation() {
        return annotation;
    }

    public Class<?> getInvalidClass() {
        return invalidClass;
    }

    public Field getInvalidField() {
        return invalidField;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(@NonNull final String message) {
        this.message = message;
    }
}
