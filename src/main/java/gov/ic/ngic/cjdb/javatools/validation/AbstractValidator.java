package gov.ic.ngic.cjdb.javatools.validation;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.lang.reflect.Field;
import java.util.List;

public abstract class AbstractValidator<T> {
    private Object object;
    private Field field;
    private List<ValidationError> errors;

    @SuppressWarnings("unchecked")
    void validate(@NonNull final Object object, @NonNull final Field field, @NonNull final List<ValidationError> errors) {
        this.object = object;
        this.field = field;
        this.errors = errors;

        try {
            validate((T) field.get(object));
        } catch (IllegalAccessException ignored) { }
    }

    public abstract void validate(@Nullable final T targetFieldObject);

    protected final void isInvalid() {
        errors.add(buildValidationError());
    }

    protected final void isInvalid(@NonNull final String string) {
        final ValidationError error = buildValidationError();
        error.setMessage(string);

        errors.add(error);
    }

    private ValidationError buildValidationError() {
        return new ValidationError(field.getAnnotation(CustomValidator.class), object.getClass(), field);
    }
}
