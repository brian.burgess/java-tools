package gov.ic.ngic.cjdb.javatools.utils;

import gov.ic.ngic.cjdb.javatools.util.ExtendedMath;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExtendedMathTests {
    @Test
    public void roundWorks() {
        assertEquals(ExtendedMath.round(1.877199, .125), 1.875, .0001);
        assertEquals(ExtendedMath.round(47, 5), 45);
        assertEquals(ExtendedMath.round(6349.4481, 5280), 5280, .0001);
    }
}
