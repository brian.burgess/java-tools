package gov.ic.ngic.cjdb.javatools.validation;


class ValidatorObject {
    @Require
    String required;

    @Require(ifFieldsNotPresent = {"fieldB"})
    String fieldA;

    @Require(ifFieldsNotPresent = {"fieldA"})
    String fieldB;

    @Require(ifFieldsPresent = {"fieldA"})
    String childOfFieldA;

    @Min(value = 5, require = true)
    String requiredMin;

    @Max(value = 10, require = true)
    String requiredMax;

    @Min(value = 0)
    @Max(value = 10)
    int minMax;

    @RegEx("valid")
    String regex;

    @CustomValidator(NullValidator.class)
    String custom;
}
