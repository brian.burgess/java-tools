package gov.ic.ngic.cjdb.javatools.validation;

public class NullValidator extends AbstractValidator<String> {
    @Override
    public void validate(String targetFieldObject) {
        if (targetFieldObject == null) {
            isInvalid("Target field must not be null!!!");
        }
    }
}
