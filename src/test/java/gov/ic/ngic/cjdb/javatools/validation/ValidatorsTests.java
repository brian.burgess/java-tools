package gov.ic.ngic.cjdb.javatools.validation;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ValidatorsTests {
    @Test
    public void correctlyValidatesRequires() {
        final ValidatorObject valid = createValidObject();
        assertEquals(Validators.validate(valid).size(), 0);

        valid.fieldA = null;
        valid.childOfFieldA = "";
        assertEquals(Validators.validate(valid).size(), 0);

        valid.fieldA = "123";
        assertEquals(Validators.validate(valid).size(), 1);

        valid.fieldB = "";
        valid.fieldA = "";
        assertEquals(Validators.validate(valid).size(), 2);
    }

    @Test
    public void correctlyUsesRequiredForOtherAnnotations() {
        final ValidatorObject valid = createValidObject();
        valid.regex = "";
        valid.requiredMin = null;

        assertEquals(Validators.validate(valid).size(), 1);
    }

    @Test
    public void correctlyValidatesRegex() {
        final ValidatorObject valid = createValidObject();
        valid.regex = "invalid";

        assertEquals(Validators.validate(valid).size(), 1);
    }

    @Test
    public void correctlyValidatesMinMax() {
        final ValidatorObject valid = createValidObject();
        valid.requiredMin = "1";
        valid.requiredMax = "12345678901234567890";

        assertEquals(Validators.validate(valid).size(), 2);
    }

    @Test
    public void correctlyUsesCustomValidator() {
        final ValidatorObject valid = createValidObject();
        valid.custom = null;

        assertEquals(Validators.validate(valid).size(), 1);
    }

    private ValidatorObject createValidObject() {
        final ValidatorObject valid = new ValidatorObject();

        valid.required = "y";
        valid.childOfFieldA = "y";
        valid.fieldA = "y";
        valid.fieldB = "y";
        valid.minMax = 10;
        valid.requiredMin = "asdfg";
        valid.requiredMax = "asdfg";
        valid.regex = "valid";
        valid.custom = "";

        return valid;
    }
}
