package gov.ic.ngic.cjdb.javatools.utils;

import gov.ic.ngic.cjdb.javatools.util.ResponseUtils;
import org.junit.Test;
import org.springframework.data.domain.Page;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class ResponseUtilsTests {
    @Test
    public void paginationWorks() {
        assertEquals(ResponseUtils.paginate(Collections.emptyList()).getTotalElements(), 0);
        assertEquals(ResponseUtils.paginate(Collections.emptyList()).getTotalPages(), 1);

        final List<String> strings = IntStream.range(0, 100).mapToObj(i -> "" + i).collect(Collectors.toList());

        final Page<String> limitlessPage = ResponseUtils.paginate(strings);
        assertEquals(limitlessPage.getTotalPages(), 1);
        assertEquals(limitlessPage.getTotalElements(), 100);


        final Page<String> firstPage = ResponseUtils.paginate(strings, 0, 25);
        assertEquals(firstPage.getTotalElements(), 100);
        assertEquals(firstPage.getTotalPages(), 4);
        assertEquals(firstPage.getSize(), 25);
        assertFalse(firstPage.hasPrevious());
        assertTrue(firstPage.hasNext());
        assertTrue(firstPage.hasContent());
    }
}
